package ru.kuzin.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.endpoint.ISystemEndpoint;
import ru.kuzin.tm.dto.request.ServerAboutRequest;
import ru.kuzin.tm.dto.request.ServerVersionRequest;
import ru.kuzin.tm.dto.response.ServerAboutResponse;
import ru.kuzin.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractClient implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getEmail());
        System.out.println(serverAboutResponse.getName());

        final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}