package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ServerAboutRequest;
import ru.kuzin.tm.dto.request.ServerVersionRequest;
import ru.kuzin.tm.dto.response.ServerAboutResponse;
import ru.kuzin.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}